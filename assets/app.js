const app = {
  vegEls: null,
  init: async function() {
    const resp = await fetch('assets/veggies.json');
    const veggies = await resp.json();
    const parent = document.querySelector('tbody');
    for (const veg of veggies) {
      const tr = document.createElement('tr');
      tr.classList.add(app.slugify(veg.name));
      tr.innerHTML = `<td>${veg.name}</td><td>${veg.cuissonVapeur}</td><td>${veg.semis}</td>`;
      parent.append(tr);
    }
    app.vegEls = parent.querySelectorAll('tr');
    app.addEvents();
  },
  addEvents: function() {
    document.querySelector('#search').addEventListener('keyup', app.handleSearch);
  },
  handleSearch: function(e) {
    const userInput = e.currentTarget.value;
    if (userInput !== '') {
      app.hideAllVeggies();
      const searching = app.slugify(userInput);
      const matchingEls = document.querySelectorAll(`tbody tr[class^="${searching}"], tbody tr[class*="${searching}"]`);
      if (matchingEls) {
        for (const el of matchingEls) {
          el.removeAttribute('hidden');
        }
      }
    } else {
      app.showAllVeggies();
    }
  },
  hideAllVeggies: function() {
    for (const el of app.vegEls) {
      el.setAttribute('hidden', 'hidden');
    }
  },
  showAllVeggies: function() {
    for (const el of app.vegEls) {
      el.removeAttribute('hidden');
    }
  },
  slugify: function(string) {
    // https://gist.github.com/hagemann/382adfc57adbd5af078dc93feef01fe1
    const a = 'àáâäæãåāăąçćčđďèéêëēėęěğǵḧîïíīįìıİłḿñńǹňôöòóœøōõőṕŕřßśšşșťțûüùúūǘůűųẃẍÿýžźż·/_,:;'
    const b = 'aaaaaaaaaacccddeeeeeeeegghiiiiiiiilmnnnnoooooooooprrsssssttuuuuuuuuuwxyyzzz------'
    const p = new RegExp(a.split('').join('|'), 'g')
    return string.toString().toLowerCase()
      .replace(/\s+/g, '-') // Replace spaces with -
      .replace(p, c => b.charAt(a.indexOf(c))) // Replace special characters
      .replace(/&/g, '-and-') // Replace & with 'and'
      .replace(/[^\w\-]+/g, '') // Remove all non-word characters
      .replace(/\-\-+/g, '-') // Replace multiple - with single -
      .replace(/^-+/, '') // Trim - from start of text
      .replace(/-+$/, '') // Trim - from end of text
  },
};

document.addEventListener('DOMContentLoaded', app.init);